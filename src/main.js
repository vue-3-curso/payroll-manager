import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import { initializeApp } from "firebase/app";
import "semantic-ui-css/semantic.min.css"
import firebaseConfig from "./utils/firebase"


const app=createApp(App);
const pinia=createPinia();

initializeApp(firebaseConfig);

app.use(router)
app.use(pinia);
app.mount('#app')