import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AuthView from '../views/AuthView.vue'
import {useAuth} from '../store/auth'

const routes = [
  {path: '/',name: 'home',component: HomeView,meta:{requireAuth:true}},
  {path: '/auth',name: 'auth',component: AuthView,meta:{requireAuth:false}},
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from,next)=>{
  const auth=useAuth()
  const isAuth=auth.token

  if(to.meta.requireAuth && (isAuth==null || isAuth==undefined)){
    next('auth')
  }else{
    next()
  }

})


export default router